using System.ComponentModel.DataAnnotations;

public class LoginModel 
{
  #region Properties
  [Required]
  public string Username { get; set; }

  [Required]
  public string Password { get; set; }
  #endregion

  #region Constructor
  public LoginModel()
  {
    Username = string.Empty;
    Password = string.Empty;
  }
  #endregion
}