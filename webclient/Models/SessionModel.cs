public class SessionModel
{
  #region Properties
  public string Token { get; set; } 
  #endregion

  #region Constructor
  public SessionModel() { 
    Token = string.Empty;
  }
  #endregion
}