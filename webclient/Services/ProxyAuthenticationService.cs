using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.JSInterop;

public class ProxyAuthenticationService:  IAuthenticationService
{
  #region Fields
  private IJSRuntime _js;
  #endregion

  #region Constructor
  public ProxyAuthenticationService(IJSRuntime js)
  {
    _js = js;
  }
  #endregion

  #region Methods
  public async Task StartSession()
  {
    SessionModel session = new SessionModel() { Token = "testtoken" };

    await _js.InvokeVoidAsync("localStorage.setItem", "session", 
      JsonSerializer.Serialize(session));
  }

  public bool IsAuthentic(LoginModel creds)
  {
    bool result = false;

    if (creds.Username == "admin" & creds.Password == "Testing123!!!")
      result = true;

    return result;
  }

  public bool IsAuthentic(SessionModel session)
  {
    bool result = false;

    if (session.Token != string.Empty)
      result = true;

    return result;
  }

  public async Task<bool> IsAuthenticAsync(LoginModel creds)
  {
    return await new Task<bool>(() => { return false; });
  }

  public async Task<bool> IsAuthenticAsync(SessionModel session)
  {
    return await new Task<bool>(() => { return false; });
  }

  public async Task<SessionModel> GetSession<SessionModel>()
  {
    SessionModel session = default(SessionModel);
    var sessionJson = await _js.InvokeAsync<string>("localStorage.getItem", 
      "session");    

    if (sessionJson != null)
      session = JsonSerializer.Deserialize<SessionModel>(sessionJson);

    return session;
  }

  public async Task EndSession() 
  {
    SessionModel session = new SessionModel();

    await _js.InvokeVoidAsync("localStorage.setItem", "session", 
      JsonSerializer.Serialize(session));
  }
  #endregion
}