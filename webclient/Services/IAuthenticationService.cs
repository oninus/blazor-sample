using System.Threading.Tasks;

public interface IAuthenticationService
{
  #region Methods
  Task StartSession();

  bool IsAuthentic(LoginModel creds);

  Task<bool> IsAuthenticAsync(LoginModel creds);

  bool IsAuthentic(SessionModel session);

  Task<bool> IsAuthenticAsync(SessionModel session);

  Task<SessionModel> GetSession<SessionModel>();

  Task EndSession();
  #endregion
}