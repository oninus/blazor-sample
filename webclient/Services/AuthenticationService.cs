using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Microsoft.JSInterop;

public class AuthenticationService:  IAuthenticationService
{
  #region Fields
  private IJSRuntime _js;
  private string _serviceUrl;
  private HttpClient _http;
  private SessionModel _session;
  #endregion

  #region Constructor
  public AuthenticationService(IJSRuntime js)
  {
    _js = js;
    _http = new HttpClient();
    _serviceUrl = "http://localhost:5050/authenticate";
    _session = new SessionModel();
  }
  #endregion

  #region Methods
  public async Task StartSession()
  {
    await _js.InvokeVoidAsync("localStorage.setItem", "session", 
        JsonSerializer.Serialize(_session));    
  }

  public bool IsAuthentic(LoginModel model)
  {
    return false;
  }

  public async Task<bool> IsAuthenticAsync(LoginModel creds)
  {
    SessionModel session = new SessionModel();
    bool result = false;
    var request = new HttpRequestMessage(HttpMethod.Post, _serviceUrl + "/login");

    request.Content = new StringContent(JsonSerializer.Serialize(creds), 
      Encoding.UTF8, "application/json");

    using var response = await _http.SendAsync(request);

    if (response.IsSuccessStatusCode)
    {
      session = await response.Content.ReadFromJsonAsync<SessionModel>();

      if (session.Token != string.Empty)
        result = true;
      else
        result = false;
    } 
    else 
    {
      session = new SessionModel();
      result = false;
    }
      
    _session = session;

    return result;
  }

  public async Task<bool> IsAuthenticAsync(SessionModel session)
  {
    bool result = false;
    var request = new HttpRequestMessage(HttpMethod.Get, _serviceUrl + "/verify");

    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer",
      session.Token);

    using var response = await _http.SendAsync(request);

    if (response.IsSuccessStatusCode)
    {
      result = await response.Content.ReadFromJsonAsync<bool>();      
    } 
    else 
    {      
      result = false;
    }

    return result;
  }

  public bool IsAuthentic(SessionModel session)
  {
    bool result = false;

    if (session.Token != string.Empty)
      result = true;

    return result;
  }

  public async Task<SessionModel> GetSession<SessionModel>()
  {
    SessionModel session = default(SessionModel);
    var sessionJson = await _js.InvokeAsync<string>("localStorage.getItem", 
      "session");    

    if (sessionJson != null)
      session = JsonSerializer.Deserialize<SessionModel>(sessionJson);

    return session;
  }

  public async Task EndSession() 
  {
    SessionModel session = new SessionModel();

    await _js.InvokeVoidAsync("localStorage.setItem", "session", 
      JsonSerializer.Serialize(session));
  }
  #endregion
}