public class Credentials
{
  public string Username { get; set; }
  public string Password { get; set; }

  public Credentials()
  {
    Username = string.Empty;
    Password = string.Empty;
  }
}