public class Session
{
  public string Token { get; set; }

  public Session()
  {
    Token = string.Empty;
  }
}