public class TokenVarsOptions
{
  public const string TokenVars = "TokenVars";

  public string Key { get; set; }

  public string Issuer { get; set; }
}