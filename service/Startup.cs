using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Eap.BlazorSample.Data.Models;

namespace service
{
    public class Startup
    {
        private IConfigurationSection _tokenVarsConfigSection;
        private TokenVarsOptions _tokenVarsOptions;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _tokenVarsConfigSection = Configuration.GetSection(
                TokenVarsOptions.TokenVars);
            _tokenVarsOptions = new TokenVarsOptions();
            _tokenVarsConfigSection.Bind(_tokenVarsOptions);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UserContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("UserContextDb")));
            services.Configure<TokenVarsOptions>(_tokenVarsConfigSection);
            SetupJWTServices(services);
            services.AddCors();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // if (env.IsDevelopment())
            // {
            //     app.UseDeveloperExceptionPage();
            // }

            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseStatusCodePages();  
            app.UseAuthentication();
            app.UseAuthorization();            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void SetupJWTServices(IServiceCollection services)  
        {
            // string key = "1q2w3e4r5t6y7u8i9o0p";
            // string issuer = "http://localhost";
            string key = _tokenVarsOptions.Key;
            string issuer = _tokenVarsOptions.Issuer;

            services.AddAuthentication(
                JwtBearerDefaults.AuthenticationScheme
            ).AddJwtBearer(
                options =>  
                {  
                    options.TokenValidationParameters = new TokenValidationParameters  
                    {  
                        ValidateIssuer = true,  
                        ValidateAudience = true,  
                        ValidateIssuerSigningKey = true,  
                        ValidIssuer = issuer,  
                        ValidAudience = issuer,  
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(key)
                        )  
                    };  
                }
            );  
        }
    }
}
