using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Eap.BlazorSample.Data.Models;

namespace service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthenticateController : ControllerBase
    {
        private readonly UserContext _dbContext;
        private readonly TokenVarsOptions _tokenVars;

        public AuthenticateController(UserContext context, 
            IOptions<TokenVarsOptions> tokenVarsSettings)
        {
            _dbContext = context;
            _tokenVars = tokenVarsSettings.Value;
        }

        private string GenerateToken(Credentials creds)
        {
            // string key = "1q2w3e4r5t6y7u8i9o0p";
            // string issuer = "http://localhost";
            string key = _tokenVars.Key;
            string issuer = _tokenVars.Issuer;
            string token = string.Empty;
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));    
            var credentials = new SigningCredentials(securityKey, 
                SecurityAlgorithms.HmacSha256);
            var permClaims = new List<Claim>();

            permClaims.Add(new Claim("name", creds.Username));

            var jwtTokenObj = new JwtSecurityToken(issuer,    
                    issuer,
                    permClaims,    
                    expires: DateTime.Now.AddDays(1),    
                    signingCredentials: credentials
            );

            token = new JwtSecurityTokenHandler().WriteToken(jwtTokenObj); 

            return token;
        }

        [HttpPost("login")]        
        public Session Login(Credentials creds)
        {
            Session session = new Session();

            try
            {
                var user = (
                    from u in _dbContext.Users
                    where u.Username == creds.Username & u.Password == creds.Password
                    select u
                ).FirstOrDefault();

                if (user != null)
                {
                    session.Token = GenerateToken(creds);
                }                         
            }
            catch
            {
                session = new Session();
            }

            return session;
        }

        [Authorize]
        [HttpGet("verify")]  
        public bool Verify() 
        {              
            return true;
        } 
    }
}
