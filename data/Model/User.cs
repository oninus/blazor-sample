using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Eap.BlazorSample.Data.Models
{
  public class User
  {    
    public int Id { get; set; }
    public string Username { get; set; }

    public string Password { get; set; }

    public List<User> Users { get; } = new List<User>();
  }
}