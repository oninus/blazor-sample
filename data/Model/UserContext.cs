using System.Collections.Generic;
using System.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Eap.BlazorSample.Data.Models
{
  public class UserContext:  DbContext
  {
    #region Properties
    public DbSet<User> Users { get; set; }    
    #endregion

    #region Constructor
    public UserContext(DbContextOptions<UserContext> options) : base(options) {}
    #endregion
    
    #region Methods
    // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    // {      
    //   // optionsBuilder.UseSqlite("Data Source=/home/oninus/Documents/Projects/DotNet/Eap/blazor-sample/service/usercontext.db");
    //   optionsBuilder.UseSqlite(
    //     ConfigurationManager.ConnectionStrings["UserContextDb"].ConnectionString
    //   );
    // }    
    #endregion
    
  }
}