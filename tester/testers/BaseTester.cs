using System;

public abstract class BaseTester
{
  #region Fields
  private bool _isSuccess;
  private string _testName;
  #endregion

  #region Constructor
  public BaseTester(string testName) 
  {      
    _isSuccess = false;
    _testName = testName;
  }
  #endregion

  #region Methods
  protected abstract bool Execute();

  public abstract void Setup();  

  public void Test() {
    Console.WriteLine("--Start {0}--", _testName);

    _isSuccess = this.Execute();

    if (!_isSuccess)    
      Console.WriteLine("Test failed!");
    else
      Console.WriteLine("Test Successful!");

    Console.WriteLine("--End {0}--", _testName);
  }
  #endregion

}