using System.Linq;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Eap.BlazorSample.Data.Models;

public class UserContextTester:  BaseTester
{
  private string _username;

  public UserContextTester():  base("UserContextTester") 
  {
    _username = string.Empty;
  }

  protected override bool Execute()
  {
    bool result = false;
    string connString = ConfigurationManager.ConnectionStrings[
      "UserContextDb"
    ].ConnectionString;
    var optionsBuilder = new DbContextOptionsBuilder<UserContext>();

    optionsBuilder.UseSqlite(connString);

    using (UserContext db = new UserContext(optionsBuilder.Options))
    {
      var user = from u in db.Users
        where u.Username == _username
        select u;

      result = (user.First().Username == "admin");
    }

    return result;
  }

  public override void Setup()
  {
    _username = "admin";
  }  
}