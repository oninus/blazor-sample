﻿using System;
using System.Collections.Generic;

namespace tester
{
    class Program
    {
        static void Main(string[] args)
        {
            List<BaseTester> testers = new List<BaseTester>();

            testers.Add(new UserContextTester());

            Console.WriteLine("--Starting Testers--");           
            foreach(BaseTester tester in testers)
            {
                tester.Setup();
                tester.Test();
            }
            Console.WriteLine("--Finished--");
        }
    }
}
